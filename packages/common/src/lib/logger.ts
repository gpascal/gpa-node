import chalk, { ChalkInstance } from 'chalk';
import moment from 'moment';

import { Maybe } from './types';
import { getCallerInfo } from './reflection-utils';

function printLog(message: string, color: ChalkInstance): void {
    const caller = getCallerInfo(4);
    const timestamp = moment().format('YYYY-MM-DD hh:mm:ss.sss');
    process.stdout.write(color(
        `${timestamp} [${caller.file}:${caller.line}]{${caller.function}} ${message}\n`,
    ));
}

type Logger = (...messages: string[]) => void;
export enum LoggerLevel {
    debug = 'debug',
    log = 'log',
    info = 'info',
    success = 'success',
    warn = 'warn',
    error = 'error',
    lifecycle = 'lifecycle',
}
const levelToColor: { [k in LoggerLevel]: ChalkInstance } = {
    debug: chalk.gray,
    log: chalk.white,
    info: chalk.cyan,
    success: chalk.green,
    warn: chalk.yellow,
    error: chalk.red,
    lifecycle: chalk.magenta,
} as const;

type LoggerType = { [k in LoggerLevel]: Logger };

// eslint-disable-next-line arrow-body-style
export const logger: LoggerType = Object
    .values(LoggerLevel)
    .reduce((agg: LoggerType, level: LoggerLevel): LoggerType => ({
        ...agg,
        [level]: (...messages: unknown[]): void => {
            const message = messages
                .map(m => (typeof m === 'string' ? m : JSON.stringify(m)))
                .join(' ');

            printLog(message, levelToColor[level]);
        },
    }), {} as LoggerType);

export const loggerTemplates = {
    block: (level: LoggerLevel, header: string, body: Maybe<string>): void => {
        const blockWidth = 64;
        logger[level](`
        ${'@'.repeat(blockWidth)}
        @ ${header.padEnd(blockWidth - 4, ' ')} @
        ${'@'.repeat(blockWidth)}

        ${body}

        ${'@'.repeat(blockWidth)}
        `);
    },
};
