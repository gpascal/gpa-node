export type Maybe<T> = T | null | undefined;

export type Task<T> = () => Promise<T> | T;

export type TaskExecutor<T> = (task: Task<T>) => Promise<T>;
