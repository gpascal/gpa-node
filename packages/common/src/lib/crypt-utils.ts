import * as crypto from 'crypto';
import * as bcrypt from 'bcrypt';

export type Encrypted = {
    hash: string,
    salt: string,
};

export class Crypto {
    public static generateSalt(): string {
        return crypto.randomBytes(64).toString('base64');
    }

    public static hash(password: string): Promise<string> {
        return bcrypt.hash(password, 10);
    }

    public static compareHash(password: string, hash: string): Promise<boolean> {
        return bcrypt.compare(password, hash);
    }

    private readonly secretKey: string;

    constructor(secretKey: string) {
        this.secretKey = secretKey;
    }

    public encrypt(token: string): Encrypted {
        // console.time('Encrypting');
        const salt = Crypto.generateSalt();
        const key = crypto.scryptSync(this.secretKey, salt, 24);
        const iv = Buffer.alloc(16);
        const cipher = crypto.createCipheriv('aes-192-cbc', key, iv);
        const encrypted = cipher.update(token, 'utf8', 'base64');
        const final = encrypted + cipher.final('base64');
        // console.timeEnd('Encrypting');
        return {
            hash: final,
            salt,
        };
    }

    public decrypt(encrypted: Encrypted): string {
        // console.time('Decrypting');
        const key = crypto.scryptSync(this.secretKey, encrypted.salt, 24);
        const initVector = Buffer.alloc(16);
        const decipher = crypto.createDecipheriv('aes-192-cbc', key, initVector);
        const decrypted = decipher.update(encrypted.hash, 'base64', 'utf8');
        const final = decrypted + decipher.final();
        // console.timeEnd('Decrypting');
        return final;
    }
}
