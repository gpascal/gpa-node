import { performance } from 'perf_hooks';

export const now = (): number => performance.now();

export type Timer = {
    getMs: () => number,
    reset: () => void,
};

export const createTimer = (): Timer => {
    let tStart = now();
    return {
        getMs: () => now() - tStart,
        reset: () => { tStart = now(); },
    };
};
