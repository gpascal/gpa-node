import * as crypto from 'crypto';

export function generateId(prefix?: string, length = 32): string {
    return (prefix ? `${prefix}_` : '')
        + base64Id(length);
}

function base64Id(length: number): string {
    return crypto
        .randomBytes(Math.ceil(length * 0.75))
        .toString('base64')
        .slice(0, length);
}
