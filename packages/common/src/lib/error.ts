export class AbstractError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = this.constructor.name;
        Error.captureStackTrace(this, this.constructor);
        this.stack = filterStack(this.stack);
    }
}

const filterStack = (stack: string | undefined): string => (stack || '')
    .split('\n')
    .filter((line, index) => index === 0 || !line.includes('internal/process'))
    .join('\n');
