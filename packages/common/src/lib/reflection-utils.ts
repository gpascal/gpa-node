import { AbstractError } from './error';

export type CallerInfo = {
    file: string,
    line: string,
    function: string,
};

export function getCallerInfo(stackDepth = 0): CallerInfo {
    const e = new AbstractError();
    // + 2 to the stackDepth because the first line is "{ErrorName}:" and the second is the "getCallerInfo()" stack entry
    // so with stackDepth = 1, we return info of the caller of the direct caller of getCallerInfo()
    const frame = e.stack?.split('\n')[stackDepth + 2] ?? '';
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const file = frame.split(':')[0]!.replace(/^.*\//, '');
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const line = frame.split(':')[1]!;
    const functionName = frame.split(' ')[5] ?? '<unknown>';

    return { file, line, function: functionName };
}
