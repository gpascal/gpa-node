import { expect } from 'chai';
import { getCallerInfo, Task } from '../lib';

describe('# reflection-utils', () => {
    it('> return the correct caller name', () => {
        function callerFunction() {
            return getCallerInfo();
        }

        const callerInfo = callerFunction();
        expect(callerInfo.function).to.equal(callerFunction.name);
    });

    it('> return the caller of the caller', () => {
        function wrappingFunction(task: Task<unknown>) {
            const callerInfo = getCallerInfo();
            expect(callerInfo.function).to.equal(wrappingFunction.name);
            task();
        }

        function innerFunction() {
            const callerInfo = getCallerInfo();
            const parentInfo = getCallerInfo(1);
            expect(callerInfo.function).to.equal(innerFunction.name);
            expect(parentInfo.function).to.equal(wrappingFunction.name);
        }

        wrappingFunction(innerFunction);
    });
});
