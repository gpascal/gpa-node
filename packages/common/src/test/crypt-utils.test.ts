import { expect } from 'chai';
import { Crypto } from '../lib';

describe('# crypt-utils', () => {
    describe('> encrypt', () => {
        it('> encrypt + decrypt', async () => {
            const token = 'abcdefghijklmnopqrstuvwxyz';
            const privateKey = Crypto.generateSalt();
            const crypto = new Crypto(privateKey);
            const encrypted = crypto.encrypt(token);
            // console.log(`ENCRYPTED=`, encrypted);
            expect(crypto.decrypt(encrypted)).to.equal(token);
        });
    });

    describe('> hash', () => {
        it('> hash + compare', async () => {
            const token = 'abcdefghijklmnopqrstuvwxyz';
            const hash = await Crypto.hash(token);
            // console.log(`HASH=${hash}`);
            expect(await Crypto.compareHash(token, hash)).to.equal(true);
        });
    });
});
