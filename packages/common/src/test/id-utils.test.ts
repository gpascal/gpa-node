import { expect } from 'chai';
import * as _ from 'lodash';

import { generateId, logger, createTimer } from '../lib';

describe('# id-utils', () => {
    describe('> generateId', () => {
        it('> simple id', () => {
            const id = generateId();
            expect(id).to.be.a('string').with.length.above(0);
        });

        it('> id with length=64', () => {
            const id = generateId(undefined, 64);
            expect(id).to.be.a('string').with.lengthOf(64);
        });

        it('> prefixed id', () => {
            const id = generateId('entity', 16);
            expect(id).to.match(/entity_.{16}/);
        });

        it('> generate 10000 IDs in less than 100ms', () => {
            const runs = 10000;
            const generateIdTimer = createTimer();
            _.times(runs, () => generateId());
            const took = generateIdTimer.getMs();
            logger.success(`Generated ${runs} IDs in ${Math.round(took)}ms (${((took * 1000) / runs).toPrecision(2)} µs/id)`);
            expect(took).to.be.below(100);
        });
    });
});
