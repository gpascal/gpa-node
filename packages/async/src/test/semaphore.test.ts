import { expect } from 'chai';
import * as _ from 'lodash';

import { createTimer, logger } from '@gpa/common';

import { createSemaphore, parallel, sleep } from '../lib';

describe('# semaphore', () => {
    describe('> createSemaphore', () => {
        it('> limit the concurrency of the critical section', async () => {
            // You can play with these parameters (while keeping concurrency < numberOfTasks)
            const testOpts = {
                concurrency: 10,
                numberOfTasks: 100,
                taskDurationMs: 25,
                enableLog: false,
            };

            const semaphore = createSemaphore(testOpts.concurrency);
            const simulateAsyncTask = async (taskId: number) => {
                (testOpts.enableLog) && logger.log(`Starting task ${taskId}`);
                await sleep(testOpts.taskDurationMs);
                (testOpts.enableLog) && logger.log(`              ${taskId} done`);
            };
            const semaphoreTimer = createTimer();
            await parallel(
                ..._.times(
                    testOpts.numberOfTasks,
                    index => () => semaphore(() => simulateAsyncTask(index)),
                ),
            );
            await semaphore.drain();

            // 5 tasks taking 10ms each, executed 2 at a times
            // Must take at least 3*10ms (otherwise it means that more than 2 tasks executed at the same time)
            // Must take less than 5*10ms (otherwise it means that the tasks have been executed sequentially)
            expect(semaphoreTimer.getMs())
                .to.be.above(Math.ceil(testOpts.numberOfTasks / testOpts.concurrency) * testOpts.taskDurationMs)
                .and.below(testOpts.numberOfTasks * testOpts.taskDurationMs);
        });
    });
});
