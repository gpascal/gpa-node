import { expect } from 'chai';

import { sleep, timeout, TimeoutError } from '../lib';

describe('# timeout', () => {
    it('> throw an error if the task takes longer than the timeout', async () => {
        try {
            await timeout(() => sleep(100), 90);
            expect.fail(`An error should have been thrown`);
        } catch (e) {
            expect(e).to.be.instanceof(TimeoutError);
        }
    });

    it('> custom error message', async () => {
        const message = 'custom timeout error message';
        try {
            await timeout(() => sleep(100), 90, message);
            expect.fail(`An error should have been thrown`);
        } catch (e) {
            expect(e).to.be.instanceof(TimeoutError);
            expect((e as TimeoutError).message).to.equal(message);
        }
    });
});
