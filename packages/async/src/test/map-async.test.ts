import { expect } from 'chai';
import { mapAsync } from '../lib';

describe('# map-async', () => {
    context('> primitive iterable', () => {
        context('> sync mapper', () => {
            it('> map array with synchronous mapper: mapAsync<T, R>(T[], (e: T) => R)', async () => {
                const result = await mapAsync([1, 2, 3], n => n * 2);
                expect(result).to.eql([2, 4, 6]);
            });
        });

        context('> async mapper', () => {
            it('> map array with asynchronous mapper: mapAsync<T, R>(T[], (e: T) => Promise<R>)', async () => {
                const result = await mapAsync([1, 2, 3], async n => n * 2);
                expect(result).to.eql([2, 4, 6]);
            });
        });
    });

    context('> async iterable', () => {
        context('> sync mapper', () => {
            it('> map a Promise<T[]> with synchronous mapper: mapAsync<T, R>(Promise<T[]>, (e: T) => R)', async () => {
                const result = await mapAsync(Promise.resolve([1, 2, 3]), n => n * 2);
                expect(result).to.eql([2, 4, 6]);
            });

            it('> map a Promise<T>[] with synchronous mapper: mapAsync<T, R>(Promise<T>[], (e: T) => R)', async () => {
                const result = await mapAsync<number, number>([
                    Promise.resolve(1),
                    Promise.resolve(2),
                    Promise.resolve(3),
                ], n => n * 2);
                expect(result).to.eql([2, 4, 6]);
            });
        });

        context('> async mapper', () => {
            it('> map a Promise<T[]> with asynchronous mapper: mapAsync<T, R>(Promise<T[]>, (e: T) => Promise<R>)', async () => {
                const result = await mapAsync(Promise.resolve([1, 2, 3]), async n => n * 2);
                expect(result).to.eql([2, 4, 6]);
            });

            it('> map a Promise<T>[] with asynchronous mapper: mapAsync<T, R>(Promise<T>[], (e: T) => Promise<R>)', async () => {
                const result = await mapAsync<number, number>([
                    Promise.resolve(1),
                    Promise.resolve(2),
                    Promise.resolve(3),
                ], async n => n * 2);
                expect(result).to.eql([2, 4, 6]);
            });
        });
    });
});
