import { createTimer } from '@gpa/common';
import { expect } from 'chai';

import { parallel, sleep } from '../lib';

describe('# parallel', () => {
    it('> execute functions in parallel', async () => {
        const parallelTimer = createTimer();
        const results: number[] = [];
        await parallel(
            async () => {
                await sleep(10);
                results.push(1);
            },
            async () => {
                await sleep(15);
                results.push(2);
            },
            async () => {
                await sleep(20);
                results.push(3);
            },
            async () => {
                await sleep(5);
                results.push(4);
            },
        );
        expect(Math.round(parallelTimer.getMs())).to.be.below(25);
        expect(results).to.eql([4, 1, 2, 3]);
    });

    // This test only serves as a proof that the values returned by the parallel method are correctly inferred by Typescript.
    it('> return is type-safe', async () => {
        const [num, str, obj, fun] = await parallel(
            async () => 1,
            async () => 'a',
            async () => ({ a: 1, b: 2 }),
            async () => () => 'a',
        );

        // If parallel() type-safety is broken, the following lines will not compile:
        expect(!!num.toPrecision).to.equal(true);
        expect(!!str.padEnd).to.equal(true);
        expect(!!obj.a.toPrecision).to.equal(true);
        expect(!!fun().padEnd).to.equal(true);
    });

    describe('> parallel.withConcurrency', () => {
        it('> limit the concurrency', async () => {
            const parallelTimer = createTimer();
            await parallel.withConcurrency(1)(
                () => sleep(10),
                () => sleep(10),
                () => sleep(10),
            );
            expect(parallelTimer.getMs()).to.be.at.least(30);

            parallelTimer.reset();
            await parallel.withConcurrency(3)(
                /* eslint-disable function-paren-newline,function-call-argument-newline */
                () => sleep(10), () => sleep(10), () => sleep(10),
                () => sleep(10), () => sleep(10), () => sleep(10),
                () => sleep(10), () => sleep(10),
                /* eslint-enable function-paren-newline,function-call-argument-newline */
            );
            expect(parallelTimer.getMs()).to.be.at.least(30).and.at.most(40);
        });
    });
});
