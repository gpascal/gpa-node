export type ReturnTypes<F extends (() => unknown)[]> = {
    [K in keyof F]:
    F[K] extends () => Promise<infer R> ? R : // unwrap promises
        (F[K] extends () => (infer R) ? R : never) // handle synchronous functions
};

export type Mapper<I, O> = (input: I) => Promise<O> | O;
