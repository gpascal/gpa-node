import { Task } from '@gpa/common';

export class TimeoutError extends Error {}

export function timeout<T>(f: Task<T>, maxTimeoutMs: number, customTimeoutErrorMessage?: string): Promise<T> {
    return new Promise((resolve: (res: T) => void, reject: (err: Error) => void) => {
        let resolved = false;

        const timer = setTimeout(() => {
            if (!resolved) {
                resolved = true;
                reject(new TimeoutError(customTimeoutErrorMessage || `Operation timed out after ${maxTimeoutMs}ms`));
            }
        }, maxTimeoutMs);

        Promise.resolve(f())
            .then((res: T) => {
                if (!resolved) {
                    resolved = true;
                    clearTimeout(timer);
                    resolve(res);
                }
            })
            .catch(err => {
                if (!resolved) {
                    resolved = true;
                    clearTimeout(timer);
                    reject(err);
                }
            });
    });
}
