import { createSemaphore } from './semaphore';
import { ReturnTypes } from './types';
import { mapAsync } from './map-async';

export function parallel<F extends(() => unknown)[]>(...fns: F): Promise<ReturnTypes<F>> {
    return mapAsync(fns, f => f()) as Promise<ReturnTypes<F>>;
}

parallel.withConcurrency = (concurrency: number) => <F extends(() => unknown)[]>(...fns: F): Promise<ReturnTypes<F>> => {
    const semaphore = createSemaphore(concurrency);
    return mapAsync(fns, f => semaphore(() => f())) as Promise<ReturnTypes<F>>;
};
