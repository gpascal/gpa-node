import { promisify } from 'util';

export const sleep = (delay: number): Promise<void> => promisify(setTimeout)(delay);
