import { Mapper } from './types';
import { createSemaphore } from './semaphore';

type MapAsyncOptions = { concurrency?: number };
type AsyncIterable<T> = Promise<T>[] | Promise<T[]> | T[];

export async function mapAsync<IterableType, ReturnType>(
    iterable: AsyncIterable<IterableType>,
    mapper: Mapper<IterableType, ReturnType>,
    opts?: MapAsyncOptions,
): Promise<ReturnType[]> {
    let effectiveMapper: Mapper<IterableType, ReturnType> = mapper;
    if (typeof opts?.concurrency === 'number') {
        const semaphore = createSemaphore<ReturnType>(opts.concurrency);
        effectiveMapper = input => semaphore(() => mapper(input));
    }

    const unwrappedIterable: IterableType[] = await Promise.all(await iterable);
    return Promise.all(
        unwrappedIterable.map(element => effectiveMapper(element)),
    );
}

/* eslint-disable implicit-arrow-linebreak */
mapAsync.withConcurrency = (concurrency: number) =>
    <Iterable, Return>(iterable: AsyncIterable<Iterable>, mapper: Mapper<Iterable, Return>): Promise<Return[]> =>
        mapAsync(iterable, mapper, { concurrency });
/* eslint-enable implicit-arrow-linebreak */
