import { EventEmitter } from 'events';
import * as _ from 'lodash';
import Queue from 'double-ended-queue';
import { Task, TaskExecutor } from '@gpa/common';

class ReleaseEmitter extends EventEmitter {}
type PromiseExecutor = { resolve: () => void, reject: (err: unknown) => void };
type Drainable = { drain(): Promise<void> };

export class Semaphore implements Drainable {
    private readonly concurrency: number;
    private readonly waitingTasks: Queue<PromiseExecutor>;
    private readonly releaseEmitter: ReleaseEmitter;
    private freeSlots: number;

    constructor(concurrency = 1, queueCapacity = 10) {
        this.concurrency = concurrency;
        this.waitingTasks = new Queue<PromiseExecutor>(queueCapacity);
        this.releaseEmitter = new ReleaseEmitter();
        this.freeSlots = concurrency;

        this.releaseEmitter.on('release', () => {
            const nextTask = this.waitingTasks.shift();
            if (nextTask !== undefined) {
                nextTask.resolve();
            } else {
                this.freeSlots += 1;
            }
        });
    }

    public async acquire(): Promise<void> {
        if (this.freeSlots > 0) {
            this.freeSlots -= 1;
            return undefined;
        }

        return new Promise((resolve, reject) => {
            this.waitingTasks.push({ resolve, reject });
        });
    }

    public release(): void {
        this.releaseEmitter.emit('release');
    }

    // This method acquires locks on all the semaphore's slots and never release
    // them, effectively definitely locking the semaphore
    public async drain(): Promise<void> {
        await Promise.all(_.times(this.concurrency, () => this.acquire()));
    }
}

export function createSemaphore<T>(concurrency: number): TaskExecutor<T> & Drainable {
    const semaphore = new Semaphore(concurrency);
    const executor = async (task: Task<T>) => {
        await semaphore.acquire();
        try {
            return await task();
        } finally {
            semaphore.release();
        }
    };
    executor.drain = () => semaphore.drain();
    return executor;
}
